// https://docs.cypress.io/api/introduction/api.html

describe('Testing Todo App', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('Shows empty todo list', () => {
    cy.contains('h1', 'TODO app')
    cy.contains('No entries yet.')
  })

  it('Adds todo and checks its existence', () => {
    cy.get('input[type="text"]').type('New task 1')
    cy.get('button').click()
    cy.get('li:first-child').contains('New task 1')
  })
})
